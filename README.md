# trivia-vue

Great game for family and friends!

To play the game, go to https://trivia-vue-production.herokuapp.com

Got some components that'll make you ghasp for air.

GameMenu - Your best friend menu-component!

GamePlay - The component that is responsible for the game. The GamePlay component makes the API call for the questions. It also houses the QuizQuestion component.

QuizQuestion - Your best friend. The component that displays the question and allows you to answer them. What a dream!

QuizResults - Oh boi, here it comes! The QuizResults component shows your results. Did you do well?

AnswerReview - The goddamn component that lives in QuizResults. Each question is being carefully examined by this Godgiven component.

Have fun buddies.
