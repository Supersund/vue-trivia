import Vue from "vue";
import Router from "vue-router";
import GameMenu from "../components/GameMenu";
import GamePlay from "../components/GamePlay";
import QuizResults from "../components/QuizResults";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "GameMenu",
      component: GameMenu
    },
    {
      path: "/GamePlay",
      name: "GamePlay",
      component: GamePlay
    },
    {
      path: "/QuizResults",
      name: "QuizResults",
      component: QuizResults,
      props: true
    }
  ]
});
